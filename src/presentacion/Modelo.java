package presentacion;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import static logica.infoFile.convertHexToString;
import static logica.infoFile.getByteArrayFileContent;
import logica.Info;
import logica.infoFile;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;
import logica.Conversiones;

public class Modelo implements Runnable {

    private VistaPrincipal ventanaPrincipal;
    private Thread hiloFiles;
    private Info info;
    private Conversiones conversiones;
    private infoFile nFiles;
    private JFileChooser ofile;

    public void iniciar() {
        //[691, 520]
        getVentanaPrincipal().setSize(1080, 640);
        getVentanaPrincipal().setVisible(true);
        getInfo();
        getFile();
    }

    public Conversiones getConversiones() {
        if (conversiones == null) {
            conversiones = new Conversiones();
        }
        return conversiones;

    }

    public Info getInfo() {
        if (info == null) {
            info = new Info();
        }
        return info;
    }

    public infoFile getFile() {
        if (nFiles == null) {
            nFiles = new infoFile(this);
        }
        return nFiles;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }

    public void iniFiles() {
        hiloFiles = new Thread(this);
        hiloFiles.start();
    }

    public void archivo() {
        int resultfile = ofile.showOpenDialog(ofile);
        if (resultfile == JFileChooser.APPROVE_OPTION) {
            try {
                byte[] file = getByteArrayFileContent("", ofile.getSelectedFile());
                //System.out.println("file: " + Arrays.toString(file));
                System.out.println("file: " + file.length);
                String[] index = new String[]{"", "", "", "", "", "", "", "", "", ""};
                //JScrollPane scrollpane = new JScrollPane(table);
                System.out.println("->" + getVentanaPrincipal().getTableP().getName());
                getVentanaPrincipal().getTableP().setEnabled(true);

                Object[][] rawData = new Object[index.length][file.length];
                int col = 0, fil = 0;
                System.out.println(Hex.encodeHex(file));
                for (int i = 0; i < file.length; i++) {
                    byte b = file[i];

                    String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                    System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
                    getInfo().setHex(b);
                    getInfo().setAscii(convertHexToString);
                    System.out.println("col:" + col + " fil" + fil);
                    if (col < 10) {
                        getVentanaPrincipal().getTableP().setValueAt((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase(), fil, col);
                        rawData[fil][col] = b;
                    } else {
                        col = -1;
                        fil++;
                    }
                    col++;
                }

            } catch (Exception e) {
            }
        } else {
            System.out.println("No se cargo el archivo");
        }
    }

    public void seleccionCelda(MouseEvent evt) {
        Point point = evt.getPoint();
        int row = ventanaPrincipal.getTableP().rowAtPoint(point);
        int column = ventanaPrincipal.getTableP().columnAtPoint(point);
        System.out.println("row:" + row + " column:" + column);
//        int columnaInicial = column;
        System.out.println("tableP: " + ventanaPrincipal.getTableP().getValueAt(row, column));
        Vector datosCon = new Vector(8);
        System.out.println("La longitud del vector es: " + datosCon.capacity());

        for (int r = column; r < (datosCon.capacity() + column); r++) {
            //System.out.println("El valor de r es: "+r);
            if (r == 20) {
                //System.out.println("La columna es igual a 20");
                if (ventanaPrincipal.getTableP().getValueAt(row, r) == null) {
                    datosCon.add("00");
                } else {
                    datosCon.add(ventanaPrincipal.getTableP().getValueAt(row, r));
                }

//                column=0;
                row++;
            } else if (r > 20) {
                //System.out.println("r es mayor a 20");
                if (ventanaPrincipal.getTableP().getValueAt(row, (r - 21)) == null) {
                    datosCon.add("00");
                } else {
                    datosCon.add(ventanaPrincipal.getTableP().getValueAt(row, (r - 21)));
                }

            } else {
                if (ventanaPrincipal.getTableP().getValueAt(row, r) == null) {
                    datosCon.add("00");
                } else {
                    datosCon.add(ventanaPrincipal.getTableP().getValueAt(row, r));
                }

            }
        }
        System.out.println("Los datos son: " + datosCon);

        getConversiones().setDatos(datosCon);

        if (getVentanaPrincipal().getRadioL().isSelected()) {
            int r8l = getConversiones().int8bitsL();
            int r16l = getConversiones().int16bitsL();
            long r24l = getConversiones().long24bitsL();
            long r32l = getConversiones().long32bitsL();
            long r64l = getConversiones().long64bitsL();
            double r64dl = getConversiones().dou64bitsL();
            getVentanaPrincipal().getBits8SinSigno().setText("" + r8l);
            getVentanaPrincipal().getBits16().setText("" + r16l);
            getVentanaPrincipal().getBits32().setText("" + r32l);
            getVentanaPrincipal().getFloat64().setText("" + r64dl);

        } else if (getVentanaPrincipal().getRadioB().isSelected()) {
            int r8b = getConversiones().int8bitsB();
            int r16bb = getConversiones().int16bitsB();
            long r24bb = getConversiones().long24bitsB();
            long r32bb = getConversiones().long32bitsB();
            long r64bb = getConversiones().long64bitsB();
            double r64db = getConversiones().dou64bitsB();
            getVentanaPrincipal().getBits8SinSigno().setText("" + r8b);
            getVentanaPrincipal().getBits16().setText("" + r16bb);
            getVentanaPrincipal().getBits32().setText("" + r32bb);
            getVentanaPrincipal().getFloat64().setText("" + r64db);

        } else if (getVentanaPrincipal().getRadioL().isSelected() && getVentanaPrincipal().getRadioB().isSelected()) {
            getVentanaPrincipal().getBits8SinSigno().setText("");
            getVentanaPrincipal().getBits16().setText("");
            getVentanaPrincipal().getBits32().setText("");
            getVentanaPrincipal().getFloat64().setText("");

        }

    }

    public void iniFile() {
        try {

            ofile = new JFileChooser();
            ofile.setCurrentDirectory(new File("./temp"));
            int resultfile = ofile.showOpenDialog(ofile);
            if (resultfile == JFileChooser.APPROVE_OPTION) {
                File origignal = ofile.getSelectedFile();
                System.out.println("origignal:" + origignal);
                System.out.println("nFiles:" + nFiles);
                nFiles.splitFile(origignal.getAbsoluteFile());
            }
        } catch (Exception e) {
            System.out.println("run() e:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        iniFile();
    }
}
