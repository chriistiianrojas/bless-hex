package presentacion;

import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

public class Controlador implements ActionListener, AdjustmentListener {

    private VistaPrincipal ventana;
    private Modelo model;

    public Controlador(VistaPrincipal aThis) {
        ventana = aThis;
        model = ventana.getModel();
   
    }

    /*@Override
     public void actionPerformed(ActionEvent e) {
     JButton boton;
     if(e.getSource() instanceof JButton){
     boton = (JButton)e.getSource();
     if(boton.getName().equals("suma")){
                
     }
     }
     }*/
    public void getTable() {
        model.getVentanaPrincipal();

    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent evt) {

        model.iniFiles();
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        System.out.println("e: " + e.paramString());
    }

}
