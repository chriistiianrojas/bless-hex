/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author jorge.cardenas
 */
public class Info {

    private byte hex;
    private String Ascii;
    private JSONObject matrix;
    private JSONObject infoFile;

    public Info() {
    }

    public JSONObject getInfoFile() {
        return infoFile;
    }

    public void setInfoFile(JSONObject infoFile) {
        this.infoFile = infoFile;
    }
    private JSONArray data;
    private JSONArray files;

    public JSONArray getFiles() {
        return files;
    }

    public void setFiles(JSONArray files) {
        this.files = files;
    }

    public JSONArray getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }

    public JSONObject getMatrix() {
        return matrix;
    }

    public void setMatrix(JSONObject matrix) {
        this.matrix = matrix;
    }

    public byte getHex() {
        return hex;
    }

    public void setHex(byte hex) {
        this.hex = hex;
    }

    public String getAscii() {
        return Ascii;
    }

    public void setAscii(String Ascii) {
        this.Ascii = Ascii;
    }

}
