/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import presentacion.Modelo;

/**
 *
 * @author jorge.cardenas
 */
public class infoFile {

    private static final String dir = "tmp/";
    private static final String suffix = ".splitPart";

    private Info info;
    private Modelo modelo;

    public infoFile(Modelo modelo) {
        this.modelo = modelo;
    }

    public Info getInfo() {
        if (info == null) {
            info = new Info();
        }
        return info;
    }

    public static byte[] getByteArrayFileContent(String path, File f) {
        byte[] content = null;
        try {
            //File f = new File(path);
            FileInputStream src_file = new FileInputStream(f);
            content = new byte[(int) f.length()];
            src_file.read(content);
            src_file.close();
        } catch (Exception e) {
            System.out.println("");
        }
        return content;

    }

    public static byte[] getByteArrayFileContent(String path) {
        byte[] content = null;
        try {
            File f = new File(path);
            FileInputStream src_file = new FileInputStream(f);
            content = new byte[(int) f.length()];
            src_file.read(content);
            src_file.close();
        } catch (Exception e) {
            System.out.println("");
        }
        return content;

    }

    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        return sb.toString();
    }

    public static String completeByLeft(String str, char car, int length) {
        for (int i = str.length(); i < length; i++) {
            str = car + str;
        }
        return str;
    }

    public void archivo(java.io.File files) {

        try {

            byte[] file = getByteArrayFileContent("", files);
            //System.out.println("file: " + Arrays.toString(file));
            System.out.println("file: " + file.length);
            String[] index = new String[]{"", "", "", "", "", "", "", "", "", ""};
            //JScrollPane scrollpane = new JScrollPane(table);
            info.setMatrix(null);
            Object[][] rawData = new Object[index.length][file.length];
            int col = 0, fil = 0;
            System.out.println(Hex.encodeHex(file));
            for (int i = 0; i < file.length; i++) {
                byte b = file[i];
                String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
                System.out.println("col:" + col + " fil" + fil);
                if (col < 10) {
                    rawData[fil][col] = b;

                } else {
                    col = -1;
                    fil++;
                }
                col++;
            }

        } catch (Exception e) {
            System.out.println("archivo e: " + e.getMessage());
        }

    }

    public void splitFile(String a, String b) {
        System.out.println("a: " + a);
        System.out.println("b: " + b);
    }

    public void splitFile(File originFile) {
        String fileUrl = "", fileName = "", url = "";
        Date date;
        JSONObject infoFile = new JSONObject();
        DecimalFormat df = new DecimalFormat("#.00");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Limpiar();
            long ms = originFile.lastModified();
            date = new Date(ms);
            float longitud = originFile.length();
            fileUrl = originFile.getParent();
            fileName = originFile.getName();
            url = originFile.getAbsolutePath();
            System.out.println("originFile: " + originFile.getAbsolutePath());
            System.out.println("originFile: " + originFile.getCanonicalPath());
            System.out.println("originFile: " + originFile.getParent());
            System.out.println("originFile: " + originFile.getPath());
            infoFile.put("name", fileName);
            infoFile.put("url", originFile.getAbsolutePath());
            infoFile.put("date", output.format(date));

            if (longitud > 1024000000) {
                infoFile.put("size", df.format(longitud / 1024000000) + " GB");
            } else if (longitud > 1024000) {
                infoFile.put("size", df.format(longitud / 1024000) + " MB");
            } else if (longitud > 1024) {
                infoFile.put("size", df.format(longitud / 1024) + " KB");
            } else {
                infoFile.put("size", df.format(longitud) + "B");
            }
            modelo.getVentanaPrincipal().getFileurl().setText(infoFile.getString("url"));
            modelo.getVentanaPrincipal().getFilesize().setText(infoFile.getString("size"));
            modelo.getVentanaPrincipal().getFiledate().setText(infoFile.getString("date"));

            RandomAccessFile raf = new RandomAccessFile(url, "r");
            String nameF = fileName.substring(0, fileName.lastIndexOf('.'));
            int maxReadBufferSize = 8 * 1024; //8KB
            int maxSize = 5 * 1024 * 1014; //8KB
            long numSplits = raf.length(); //from user input, extract it from args

            if (longitud > 1024000000) {
                numSplits = raf.length() / maxSize; //from user input, extract it from args
            } else if (longitud > 1024000) {
                if (maxSize >= numSplits) {
                    numSplits = 1;
                } else {
                    numSplits = raf.length() / maxSize; //from user input, extract it from args
                }
            } else if (longitud > 1024) {
                numSplits = 1;
            } else {
                numSplits = 1;
            }
            System.out.println("numSplits: " + numSplits);
            long sourceSize = raf.length();
            long bytesPerSplit = sourceSize / numSplits;
            long remainingBytes = sourceSize % numSplits;
            JSONArray listFile = new JSONArray();
            String splitname = "";
            splitname = fileName + "split.";
            for (int destIx = 1; destIx <= numSplits; destIx++) {

                BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(splitname + destIx));
                System.out.println("bw: " + bw);
                System.out.println("bytesPerSplit:" + bytesPerSplit);
                System.out.println("maxReadBufferSize:" + maxReadBufferSize);
                if (bytesPerSplit > maxReadBufferSize) {
                    long numReads = bytesPerSplit / maxReadBufferSize;
                    long numRemainingRead = bytesPerSplit % maxReadBufferSize;
                    System.out.println("numReads:" + numReads);
                    System.out.println("numRemainingRead:" + numRemainingRead);
                    for (int i = 0; i < numReads; i++) {
                        readWrite(raf, bw, maxReadBufferSize);
                    }
                    if (numRemainingRead > 0) {
                        readWrite(raf, bw, numRemainingRead);
                    }
                } else {
                    System.out.println("raf" + raf);
                    System.out.println("bw:" + bw);
                    System.out.println("bytesPerSplit:" + bytesPerSplit);

                    readWrite(raf, bw, bytesPerSplit);
                }
                bw.close();
                listFile.put(splitname + destIx);
                System.out.println("destIx: " + destIx);
                modelo.getInfo().setFiles(listFile);
                if (destIx == 1) {
                    System.out.println("266 ");
                    Mostrar(destIx);
                } else {
                    Mostrar(destIx);
                }
            }
            if (remainingBytes > 0) {
                BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(splitname + (numSplits + 1)));
                readWrite(raf, bw, remainingBytes);
                bw.close();
            }
            raf.close();

        } catch (Exception e) {
            System.out.println("splitFile: e: " + e.getMessage());
            System.out.println("splitFile: e: " + e.getLocalizedMessage());
            System.out.println("splitFile: e: " + e.getStackTrace());
            e.printStackTrace();
        }

    }

    static void readWrite(RandomAccessFile raf, BufferedOutputStream bw, long numBytes) throws IOException {
        byte[] buf = new byte[(int) numBytes];
        int val = raf.read(buf);
        if (val != -1) {
            bw.write(buf);
        }
    }

    public synchronized void Limpiar() {
        DefaultTableModel mod = (DefaultTableModel) modelo.getVentanaPrincipal().getTableP().getModel();
        DefaultTableModel mod2 = (DefaultTableModel) modelo.getVentanaPrincipal().getjTableASCII().getModel();
        DefaultTableModel mod3 = (DefaultTableModel) modelo.getVentanaPrincipal().getjTableoffset().getModel();
        mod.getDataVector().clear();
        mod2.getDataVector().clear();
        mod3.getDataVector().clear();
    }

    public synchronized void Mostrar(int pos) {

        try {
            System.out.println("257: " + modelo.getInfo().getFiles());

            String urlFile = modelo.getInfo().getFiles().getString(pos - 1);
            Vector offset = new Vector();
            System.out.println("urlFile:" + urlFile);
            byte[] file = getByteArrayFileContent(urlFile);
            //System.out.println("file: " + Arrays.toString(file));
            System.out.println("file: " + file.length);
            String[] index = new String[]{"", "", "", "", "", "", "", "", "", ""};
            //JScrollPane scrollpane = new JScrollPane(table);
            System.out.println("->" + modelo.getVentanaPrincipal().getTableP().getName());
            modelo.getVentanaPrincipal().getTableP().setEnabled(true);
            Object[][] rawData = new Object[index.length][file.length];
            int col = 0, fil = 0, off = 0;
            System.out.println("el tamano es: " + file.length);
            DefaultTableModel mod = (DefaultTableModel) modelo.getVentanaPrincipal().getTableP().getModel();
            DefaultTableModel mod2 = (DefaultTableModel) modelo.getVentanaPrincipal().getjTableASCII().getModel();
            DefaultTableModel mod3 = (DefaultTableModel) modelo.getVentanaPrincipal().getjTableoffset().getModel();
            JSONArray data = new JSONArray();
            Object obj = null;
            Vector<String> rowOne = new Vector<String>();
            Vector<String> rowTwo = new Vector<String>();
            Vector<String> rowTree = new Vector<String>();
            modelo.getVentanaPrincipal().getTableP().setEnabled(false);
            modelo.getVentanaPrincipal().getjTableASCII().setEnabled(false);
            modelo.getVentanaPrincipal().getjTableoffset().setEnabled(false);

            String hex = "";
            for (int i = 0; i < file.length; i++) {
                byte b = file[i];
                String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                modelo.getInfo().setHex(b);
                modelo.getInfo().setAscii(convertHexToString);
                //System.out.println("3 B[" + b + "] HEX[" + (Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase() + "] file[" + fil + "] col[" + col + "]");
                if (col == 0) {
                    rowOne = new Vector<>();
                    rowTwo = new Vector<>();
                    //System.out.println("1 " + (Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase() + " file:" + fil + " col:" + col);
                    // modelo.getVentanaPrincipal().getTableP().setValueAt((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase(), fil, col);
                    //modelo.getVentanaPrincipal().getTableP().setValueAt(Integer.toHexString(off), fil, col);
                    off = off + 10;
                    hex = Integer.toHexString(b);
                    if (hex.length() == 1) {
                        hex = "0" + hex;
                    } else if (hex.length() > 2) {
                        hex = hex.substring((hex.length() - 2), hex.length());
                    }
                    rowOne.add((hex).toUpperCase());
                    rowTwo.add(convertHexToString((hex).toUpperCase()));
                    //rowTree = new Vector<>();
                    //rowTree.add(completeByLeft(Integer.toHexString(i + 1).toString(), '0', 8));
                    //mod3.addRow(rowTree);
                } else if (col < 11) {
                    //modelo.getVentanaPrincipal().getTableP().setValueAt((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase(), fil, col);
                    //modelo.getVentanaPrincipal().getTableP().setValueAt(Integer.toHexString(b), fil, col);
                    if (col < 11 && i == (file.length - 1)) {
//                            System.out.println("saltar columnas");
                        i = file.length - col - 1;
                        col = 10;
                    }
                    hex = Integer.toHexString(b);
                    if (hex.length() == 1) {
                        hex = "0" + hex;
                    } else if (hex.length() > 2) {
                        hex = hex.substring((hex.length() - 2), hex.length());
                    }
                    rowTwo.add(convertHexToString((hex).toUpperCase()));
                    rowOne.add((hex).toUpperCase());
                } else if (col < 21) {
                    //data.put((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase());
                    hex = Integer.toHexString(b);
                    if (hex.length() == 1) {
                        hex = "0" + hex;
                    } else if (hex.length() > 2) {
                        hex = hex.substring((hex.length() - 2), hex.length());
                    }
                    rowOne.add((hex).toUpperCase());
                    rowTwo.add(convertHexToString((hex).toUpperCase()));
                    if (i == (file.length - 1)) {
                        mod.addRow(rowOne);
                        mod2.addRow(rowTwo);
                        rowTree = new Vector<>();
                        rowTree.add(completeByLeft(Integer.toHexString(fil + 1).toString(), '0', 8).toUpperCase());
                        mod3.addRow(rowTree);
                    }
                    //System.out.println("3 " + (Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase() + " file:" + file + " col:" + col);
                    //modelo.getVentanaPrincipal().getTableP().setValueAt(convertHexToString, fil, col);
                    //modelo.getVentanaPrincipal().getTableP().setValueAt((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase(), fil, col);
                } else {
                    col = -1;
                    fil++;
                    obj = data;
                    mod.addRow(rowOne);
                    mod2.addRow(rowTwo);
                    rowTree = new Vector<>();
                    rowTree.add(completeByLeft(Integer.toHexString(fil).toString(), '0', 8).toUpperCase());
                    mod3.addRow(rowTree);
                    rowOne = new Vector<>();
                    rowTwo = new Vector<>();
                    data = new JSONArray();
                }
                col++;
            }
            synchronizer();

        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void synchronizer() {
        Synchronizer synchronizer = new Synchronizer(modelo.getVentanaPrincipal().getjScrollPane2(), modelo.getVentanaPrincipal().getjScrollPane3(), modelo.getVentanaPrincipal().getjScrollPane4());
        modelo.getVentanaPrincipal().getjScrollPane2().getHorizontalScrollBar().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane2().getVerticalScrollBar().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane2().getViewport().getView().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane3().getHorizontalScrollBar().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane3().getVerticalScrollBar().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane3().getViewport().getView().setEnabled(false);
        modelo.getVentanaPrincipal().getjScrollPane2().getVerticalScrollBar().addAdjustmentListener(synchronizer);
        modelo.getVentanaPrincipal().getjScrollPane2().getHorizontalScrollBar().addAdjustmentListener(synchronizer);
        modelo.getVentanaPrincipal().getjScrollPane3().getVerticalScrollBar().addAdjustmentListener(synchronizer);
        modelo.getVentanaPrincipal().getjScrollPane3().getHorizontalScrollBar().addAdjustmentListener(synchronizer);
        modelo.getVentanaPrincipal().getjScrollPane4().getVerticalScrollBar().addAdjustmentListener(synchronizer);
        modelo.getVentanaPrincipal().getjScrollPane4().getHorizontalScrollBar().addAdjustmentListener(synchronizer);
    }
}
