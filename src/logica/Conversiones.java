/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.Vector;

/**
 *
 * @author imacpiv
 */
public class Conversiones {

    private String datosM;
    private Vector datos;

    public Vector getDatos() {
        return datos;
    }

    public void setDatos(Vector datos) {
        this.datos = datos;
    }

    public int int8bitsB() {
        String datosM = datos.get(0).toString();
//        System.out.println("El primer es: "+datosM);
//        System.out.println("EL 8 bits es: "+Integer.parseInt(datosM,16));
        return Integer.parseInt(datosM, 16);
    }

    public int int16bitsB() {
        String datos16 = datos.get(0).toString() + datos.get(1).toString();
        // System.out.println("El dato 16 es: "+Integer.parseInt(datos16,16));
        return Integer.parseInt(datos16, 16);
    }

    public Long long24bitsB() {
        String datos24 = datos.get(0).toString() + datos.get(1).toString() + datos.get(2).toString();
        //System.out.println("El dato 24 es: "+Long.parseLong(datos24, 16));
        return Long.parseLong(datos24, 16);
    }

    public Long long32bitsB() {
        String datos32 = datos.get(0).toString() + datos.get(1).toString() + datos.get(2).toString() + datos.get(3).toString();
        //System.out.println("El dato 32 es: "+Long.parseLong(datos32, 16));
        return Long.parseLong(datos32, 16);
    }

    public Long long64bitsB() {
        String datos64 = datos.get(0).toString() + datos.get(1).toString() + datos.get(2).toString() + datos.get(3).toString() + datos.get(4).toString() + datos.get(5).toString() + datos.get(6).toString() + datos.get(7).toString();
        System.out.println("El dato es: " + datos64);
        long asHex = Long.parseLong(datos64, 16);
        return asHex;
    }

    public double dou64bitsB() {
        String datos64 = datos.get(0).toString() + datos.get(1).toString() + datos.get(2).toString() + datos.get(3).toString() + datos.get(4).toString() + datos.get(5).toString() + datos.get(6).toString() + datos.get(7).toString();
        // System.out.println("El dato es: "+datos64);
        long asHex = Long.parseLong(datos64, 16);
        return Double.longBitsToDouble(asHex);
    }

    public int int8bitsL() {
        String datosM = datos.get(0).toString();
//        System.out.println("El primer es: "+datosM);
//        System.out.println("EL 8 bits es: "+Integer.parseInt(datosM,16));
        return Integer.parseInt(datosM, 16);
    }

    public int int16bitsL() {
        String datos16B = datos.get(1).toString() + datos.get(0).toString();
        System.out.println("El dato 16 B es: " + Integer.parseInt(datos16B, 16));
        return Integer.parseInt(datos16B, 16);
    }

    public Long long24bitsL() {
        String datos24B = datos.get(2).toString() + datos.get(1).toString() + datos.get(0).toString();
        System.out.println("El dato 24 B es: " + Long.parseLong(datos24B, 16));
        return Long.parseLong(datos24B, 16);
    }

    public Long long32bitsL() {
        String datos32B = datos.get(3).toString() + datos.get(2).toString() + datos.get(1).toString() + datos.get(0).toString();
        return Long.parseLong(datos32B, 16);
    }

    public Long long64bitsL() {
        String datos64B = datos.get(7).toString() + datos.get(6).toString() + datos.get(5).toString() + datos.get(4).toString() + datos.get(3).toString() + datos.get(2).toString() + datos.get(1).toString() + datos.get(0).toString();
        System.out.println("El dato es: " + datos64B);
        long asHexB = Long.parseLong(datos64B, 16);
        return asHexB;
    }

    public double dou64bitsL() {
        String datos64B = datos.get(7).toString() + datos.get(6).toString() + datos.get(5).toString() + datos.get(4).toString() + datos.get(3).toString() + datos.get(2).toString() + datos.get(1).toString() + datos.get(0).toString();
        System.out.println("El dato es: " + datos64B);
        long asHexB = Long.parseLong(datos64B, 16);
        return Double.longBitsToDouble(asHexB);
    }

}
