/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

/**
 *
 * @author Christian Rojas
 */
class Synchronizer implements AdjustmentListener {

    JScrollBar v1, h1, v2, h2, v3, h3;

    public Synchronizer(JScrollPane sp1, JScrollPane sp2, JScrollPane sp3) {
        v1 = sp1.getVerticalScrollBar();
        h1 = sp1.getHorizontalScrollBar();
        v2 = sp2.getVerticalScrollBar();
        h2 = sp2.getHorizontalScrollBar();
        v3 = sp3.getVerticalScrollBar();
        h3 = sp3.getHorizontalScrollBar();
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {

        JScrollBar scrollBar = (JScrollBar) e.getSource();
        int value = scrollBar.getValue();
        JScrollBar target = null;
        if (scrollBar == v3) {
            target = v1;
        }
        if (scrollBar == h3) {
            target = h1;
        }
        if (scrollBar == v1) {
            target = v2;
        }
        if (scrollBar == h1) {
            target = h2;
        }
        if (scrollBar == v2) {
            target = v1;
        }
        if (scrollBar == h2) {
            target = h1;
        }
        target.setValue(value);
    }
}
